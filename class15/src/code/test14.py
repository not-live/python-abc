#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#pip install rospkg
#pip install pyyaml
#sudo apt install ros-noetic-usb-cam -y

import rospy
import cv2
import os
import numpy as np
import face_recognition
from sensor_msgs.msg import Image, RegionOfInterest
from cv_bridge import CvBridge, CvBridgeError
from std_msgs.msg import String

class faceDetector:
    def __init__(self):
        rospy.on_shutdown(self.cleanup);

        # 创建cv_bridge
        self.bridge = CvBridge()

        self.image_pub = rospy.Publisher("/name_recognition", String, queue_size=1)
        # 初始化订阅rgb格式图像数据的订阅者，此处图像topic的话题名可以在launch文件中重映射
        self.image_sub = rospy.Subscriber("/usb_cam/image_raw", Image, self.image_callback, queue_size=1)

    def image_callback(self, data):
        # 使用cv_bridge将ROS的图像数据转换成OpenCV的图像格式
        try:
            cv_image = self.bridge.imgmsg_to_cv2(data, "bgr8")
            frame = np.array(cv_image, dtype=np.uint8)
        except CvBridgeError as e:
            print(e)

        # 尝试检测人脸
        faces_result = self.detect_face(frame)

    def detect_face(self, input_image):
        path_know = './people_i_know'

        known_face_encodings = []
        known_face_names = []

        for path in os.listdir(path_know):
            img = face_recognition.load_image_file(path_know + '/' + path)
            encoding = face_recognition.face_encodings(img)[0]
            known_face_encodings.append(encoding)
            name = path.split('.')[0]
            known_face_names.append(name)

        # Initialize some variables
        face_locations = []
        face_encodings = []
        face_names = []
        process_this_frame = True

        while True:
            # Resize frame of video to 1/4 size for faster face recognition processing
            small_frame = cv2.resize(input_image, (0, 0), fx=0.25, fy=0.25)

            # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
            rgb_small_frame = small_frame[:, :, ::-1]
            # Only process every other frame of video to save time
            if process_this_frame:
                # Find all the faces and face encodings in the current frame of video
                face_locations = face_recognition.face_locations(rgb_small_frame)
                face_encodings = face_recognition.face_encodings(rgb_small_frame, face_locations)

                face_names = []
                for face_encoding in face_encodings:
                    # See if the face is a match for the known face(s)
                    matches = face_recognition.compare_faces(known_face_encodings, face_encoding, 0.5)

                    # If a match was found in known_face_encodings, just use the first one.
                    if True in matches:
                        first_match_index = matches.index(True)
                        name = known_face_names[first_match_index]
                        self.image_pub.publish(name)
                        break
                    else:
                        self.image_pub.publish("unknown")
                        break
                    break
                break
        return 0

    def cleanup(self):
        print("Shutting down vision node.")
        cv2.destroyAllWindows()

if __name__ == '__main__':
    try:
        # 初始化ros节点
        rospy.init_node("face_detector")
        faceDetector()
        rospy.loginfo("Face detector is started..")
        rospy.loginfo("Please subscribe the ROS Topic /name_recognition.")
        rospy.spin()
    except KeyboardInterrupt:
        print("Shutting down face detector node.")
        cv2.destroyAllWindows()